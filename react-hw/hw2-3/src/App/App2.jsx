import React, { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route, useRoutes } from "react-router-dom";
import './App.scss'
import './reset.scss'
import Modal from '../Components/Modal/Modal'
import ItemList from '../Components/ItemList/ItemList';
import Favorite from '../pages/Favorite/Favorite';
import CardPage from '../pages/CardPage.jsx/CardPage';
import Layout from '../pages/Layout';
import SingleItemPage from '../pages/SingleItemPage/SingleItemPage';

export default function App2(props) {
    const [secondModalActive, setSecondModalActive] = useState(false);
    const [firstModalActive, setFirstModalActive] = useState(false);
    const [delateOrderModal, setDelateOrderModal] = useState(false);
    const [delateWishModal, setDelateWishModal] = useState(false);

    const [items, setItems] = useState([]);
    const [wish, setWish] = useState(localStorage.wish !== null ? JSON.parse(localStorage.wish) : []);
    const [orders, setOrders] = useState(localStorage.orders !== null ? JSON.parse(localStorage.orders) : []);
    const [SelectItem, setSelectItem] = useState({});
    
    
    
    const [getItem, setcheckItem] = useState({});
    
    useEffect(() => {
        fetch('data.json')
        .then(data => data.json())
        .then(data => setItems(data))
    })
    
    const checkItem = (item) => {
        setSelectItem(item)

    }

    const showFirstModal = () => {
        setFirstModalActive(!firstModalActive)
    }

    const showSecondModal = () => {
        setSecondModalActive(!secondModalActive)
    }

    const deleteOrderModal = () => {
        setDelateOrderModal(!delateOrderModal)
    }

    const delateWishModalFunc = () => {
        setDelateWishModal(!delateWishModal)
    }

    const addOrder = (item) => {
        let isInArr = false;
        orders.forEach(el => {
            if (item.id === el.id) {
                isInArr = true
            }
        })
        if (!isInArr) {
            setOrders([...orders, item]);
            localStorage.orders = JSON.stringify([...orders, item]);
        }
    }

    const addWish = (item) => {
        let isInArr = false;
        wish.forEach(el => {
            if (item.id === el.id) {
                isInArr = true
            }
        })
        if (!isInArr) {
            setWish([...wish, item])
            localStorage.wish = JSON.stringify([...wish, item]);
        }
    }


    const delateOrder = (item) => {
        let newOrders = []
        orders.forEach(el => {
            if (item.id === el.id) {
                setSelectItem(el.id)
                return
            } else {
                newOrders.push(el)
            }
        })
        setOrders([...newOrders]);
        localStorage.orders = JSON.stringify([...newOrders]);

    }

    const delateWish = (item) => {
        let newOrders = []
        wish.forEach(el => {
            if (item.id === el.id) {
                setSelectItem(el.id)
                return
            } else {
                newOrders.push(el)
            }
        })
        setWish([...newOrders])
        localStorage.wish = JSON.stringify([...newOrders]);
    }

    return (
        <div>

            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<Layout
                        funcs={[delateOrder, delateWish, checkItem]}
                        state={[items, wish, orders]}
                        delateWish={delateWish}
                        delateOrder={delateOrder}
                        wish={wish}
                        orders={orders}
                    />}>
                        <Route index element={<ItemList
                            state={[secondModalActive, items, wish, orders, SelectItem]}
                            funcs={[showSecondModal, showFirstModal, addOrder, addWish, setcheckItem, checkItem, delateWish, delateOrder]}
                        />} />

                        <Route path='/:id' element={<SingleItemPage items={items} />} />

                        <Route path="favorite" element={<Favorite
                            state={[secondModalActive, items, wish, orders, SelectItem]}
                            funcs={[delateWishModalFunc, addOrder, checkItem, delateWish, showSecondModal]}
                        />} />
                        <Route path="card" element={<CardPage
                            state={[secondModalActive, items, wish, orders, SelectItem]}
                            funcs={[checkItem, deleteOrderModal]}

                        />} />
                    </Route>
                </Routes>
            </BrowserRouter>


            {secondModalActive && <Modal
                closeModal={showSecondModal}
                onClick={() => {
                    showSecondModal()
                    addOrder(SelectItem)
                }}
                argument={SelectItem}
                closeButton={false}
                btnText='Ок'
                modalTitle='Додати товар до корзини'
                modalText={`Якщо бажаєте додати товар ${SelectItem.category}  ${SelectItem.name} до корзини, натисніть "Ок"`}
                background='#d5d5d5' />}

            {firstModalActive && <Modal
                closeModal={showFirstModal}
                onClick={() => {
                    showFirstModal()
                    addWish(SelectItem)

                }}
                argument={SelectItem}
                closeButton={false}
                btnText='Ок'
                modalTitle='Додати товар до списку бажань'
                modalText={`Якщо бажаєте додати товар ${SelectItem.category}  ${SelectItem.name} до списку бажань, натисніть "Ок"`}
                background='#d5d5d5' />}

            {delateOrderModal && <Modal
                closeModal={deleteOrderModal}
                onClick={() => {
                    deleteOrderModal()
                    delateOrder(SelectItem)

                }}
                argument={SelectItem}
                closeButton={false}
                btnText='Ок'
                modalTitle='Видалити товар з кошика?'
                modalText={`Підтвердіть видалення товару ${SelectItem.category}  ${SelectItem.name} з кошика, натиснувши "Ок"`}
                background='#d5d5d5' />
            }

            {delateWishModal && <Modal
                closeModal={delateWishModalFunc}
                onClick={() => {
                    delateWishModalFunc()
                    delateWish(SelectItem)
                }}
                argument={getItem}
                closeButton={false}
                btnText='Ок'
                modalTitle='Видалити товар з списку бажань?'
                modalText={`Підтвердіть видалення товару ${SelectItem.category}  ${SelectItem.name} з списку бажань, натиснувши "Ок"`}
                background='#d5d5d5' />
            }
        </div>
    )

}

