let cssLink = document.documentElement.querySelector('.light-thema');
let swichBtnList = document.querySelectorAll('.swich-thema')
let swichBlock = document.querySelector('.swich-block');

if (localStorage.getItem('thema') === 'darck') {
    cssLink.href = './css/nightstyle.css';
  
    document.querySelector('.light').classList.remove('active');
    document.querySelector('.light').classList.add('close');

    document.querySelector('.darck').classList.remove('close');
    document.querySelector('.darck').classList.add('active');
} 
if(localStorage.getItem('thema') === 'light'){
    cssLink.href = './css/style.css';
    document.querySelector('.light').classList.add('active');
    document.querySelector('.light').classList.remove('close');

    document.querySelector('.darck').classList.add('close');
    document.querySelector('.darck').classList.remove('active');

}


swichBlock.addEventListener('click', function (event) {
    if (event.target.closest('img')) {
        if (cssLink.classList.contains('light-thema')) {
            cssLink.classList.remove('light-thema');
            cssLink.classList.add('darck-thema');

            cssLink.href = './css/nightstyle.css'; 

            document.querySelector('.light').classList.remove('active');
            document.querySelector('.light').classList.add('close');

            document.querySelector('.darck').classList.add('active');
            document.querySelector('.darck').classList.remove('close');
            localStorage.setItem('thema', 'darck')            
        } else{
            cssLink.classList.remove('darck-thema');
            cssLink.classList.add('light-thema');
            cssLink.href = './css/style.css';  

            document.querySelector('.darck').classList.add('close');
            document.querySelector('.darck').classList.remove('active');
           
            document.querySelector('.light').classList.add('active');
            document.querySelector('.light').classList.remove('close');
           
            localStorage.setItem('thema', 'light')
        }  
    }
})



