export default class Page {
    constructor(title, text) {
        this.title = title;
        this.text = text;

    }

    renderPage() {
        let pageContainer = document.createElement('div');
        pageContainer.classList.add('page-container');

        let contentBox = document.createElement('div');
        contentBox.classList.add('content-box');

        let title = document.createElement('h2')
        title.classList.add('page__title');
        title.innerText = this.title;

        let text = document.createElement('p')
        text.classList.add('page__text');
        text.innerText = this.text;

        let arrowBlock = document.createElement('div');
        arrowBlock.classList.add('page__arrow-block');


        let arrowLeft = document.createElement('img')
        arrowLeft.classList.add('page__arrow-block--arrow-left');
        arrowLeft.classList.add('page__arrow-block--arrow');
        arrowLeft.src = '../img/icons/arrow_left.svg';

        let arrowRight = document.createElement('img')
        arrowRight.classList.add('page__arrow-block--arrow-right');
        arrowRight.classList.add('page__arrow-block--arrow');
        arrowRight.src = '../img/icons/arrow_left.svg';

        let optionContainer = document.createElement('div');
        optionContainer.classList.add('option-container');

        
        arrowBlock.append(arrowLeft, arrowRight);

        contentBox.append(title, text, optionContainer, arrowBlock);
        pageContainer.append(contentBox);
        document.querySelector(".page-content").append(pageContainer)

    }
}
