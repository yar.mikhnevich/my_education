export default class Post {
    constructor(userId, body, title) {
        this.userId = userId;
        this.body = body;
        this.title = title;
    }

    renderPost() {
        const postItem = document.createElement("div");
        postItem.id = this.userId;
        postItem.classList.add('post-block')

        const title = document.createElement("h4");
        title.innerText = this.title;

        const bodyPost = document.createElement("p");
        bodyPost.innerText = this.body;

        const btnDelete = document.createElement("button");
        btnDelete.innerText = "Delete post"
        btnDelete.classList.add('btn-remove')
        btnDelete.id = this.userId;
        btnDelete.addEventListener('click', async function (event) {
            const target = event.target;
            if (target.closest('button')) {
                const status = await fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                    method: 'DELETE',
                })
                    .then(response => response.status)

                if (status === 200) {
                    target.closest(".post-block").remove()
                    console.log(`I remove this post, because status:${status}`);
                }
            }

        })

        const btnChange = document.createElement("button");
        btnChange.innerText = "Change post"
        btnChange.classList.add('btn-change')
        btnChange.id = this.userId;
        btnChange.addEventListener('click', function (event) {
            const target = event.target;
            if (target.closest('button')) {
                const parent = target.closest('.post-block');

                const title = parent.querySelector('h4').innerText;
                const text = parent.querySelector('p').innerText;
                const id = parent.id;
                parent.innerHTML = '';
                new Post(id, text, title).changePost(parent);

                
                parent.querySelector('.btn-confirm').addEventListener('click', 

                async function (event) {
                    const target = event.target;
                    if (target.closest('button')) {
                        const titlePost = parent.querySelector('.input-title').value;
                        const text = parent.querySelector('.textarea-post').value;
                        const id = parent.id;


                        const changes = await fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
                            method: 'PUT',
                            body: JSON.stringify({
                                userId: id,
                                body: text,
                                title: titlePost,
                            }),
                            headers: {
                              'Content-Type': 'application/json'
                            }
                          })
                          
                          .then(response => {
                            if (response.status === 200) {
                                return response.json()
                            }

                        })

                        const{userId, body, title} = changes;
                        parent.innerHTML = '';
                       new Post(userId, body, title).createNewPost(parent);
                        
                    }
                }
                )

            }

        }
        )




        postItem.append(title, bodyPost, btnDelete, btnChange);
        document.getElementById(`${this.userId}`).prepend(postItem)
    }

    changePost(parent) {

        const btnConfirmChange = document.createElement("button");
        btnConfirmChange.innerText = "Confirm changes";
        btnConfirmChange.classList.add('btn-confirm');
        btnConfirmChange.id = this.userId;

        const title = document.createElement('input');
        title.type = "text";
        title.classList.add("input-title");
        title.value = this.title;


        const text = document.createElement('textarea');
        text.classList.add('textarea-post')
        text.innerText = this.body;



        parent.append(title, text, btnConfirmChange);

    }

    createNewPost(block) {
        block.id = this.userId;
        block.classList.add('post-block')

        const title = document.createElement("h4");
        title.innerText = this.title;

        const bodyPost = document.createElement("p");
        bodyPost.innerText = this.body;

        const btnDelete = document.createElement("button");
        btnDelete.innerText = "Delete post"
        btnDelete.classList.add('btn-remove')
        btnDelete.id = this.userId;
        btnDelete.addEventListener('click', async function (event) {
            const target = event.target;
            if (target.closest('button')) {
                const status = await fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                    method: 'DELETE',
                })
                    .then(response => response.status)

                if (status === 200) {
                    target.closest(".post-block").remove()
                    console.log(`I remove this post, because status:${status}`);
                }
            }

        })



        const btnChange = document.createElement("button");
        btnChange.innerText = "Change post"
        btnChange.classList.add('btn-change')
        btnChange.id = this.userId;
        btnChange.addEventListener('click',  
        function (event) {
            const target = event.target;
            if (target.closest('button')) {
                const parent = target.closest('.post-block');

                const title = parent.querySelector('h4').innerText;
                const text = parent.querySelector('p').innerText;
                const id = parent.id;
                parent.innerHTML = '';
                new Post(id, text, title).changePost(parent);

                
                parent.querySelector('.btn-confirm').addEventListener('click', async function (event) {
                    const target = event.target;
                    if (target.closest('button')) {
                        const titlePost = parent.querySelector('.input-title').value;
                        const text = parent.querySelector('.textarea-post').value;
                        const id = parent.id;


                        const changes = await fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
                            method: 'PUT',
                            body: JSON.stringify({
                                userId: id,
                                body: text,
                                title: titlePost,
                            }),
                            headers: {
                              'Content-Type': 'application/json'
                            }
                          })
                          
                          .then(response => {
                            if (response.status === 200) {
                                return response.json()
                            }

                        })

                        const{userId, body, title} = changes;
                        parent.innerHTML = '';
                       new Post(userId, body, title).createNewPost(parent);
                        
                    }
                }
                )

            }

        }
        )
        
        block.prepend(title, bodyPost, btnDelete, btnChange);
    }
}


