Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

AJAX(Asynchronous JavaScript and XML ) це принцип використання таких технологій JS, HTML, CSS, DOM, XMLHttpRequest обєкт в комплексі, для більш гнучкого і оптимального їх використання. 
Важливо те, що використовуючи данний принцип ми можемо отримувати інформацію, яка знаходиться на сервері, а також змінювати її в фоновому режимі (не перезавантажуючи сторінку).