let btnList = document.querySelectorAll('.btn');

document.addEventListener('keydown', function (event) {
    let codeValue = event.code.slice(-1).toLowerCase();

    for (const btn of btnList) {
       btn.classList.remove('active');
       if (codeValue === btn.innerText.toLowerCase() || event.code.toLocaleLowerCase() === btn.innerText.toLowerCase()) {
        btn.classList.add('active');
       }
    }
})
