import React from 'react';
import ReactDOM from 'react-dom/client';
import './Modal.scss'
import PropTypes from 'prop-types';
import Btn from '../Btn/Btn';

export default function Modal(props) {
       return(
           <div className='modal-background'
           onClick={() => {
                props.closeModal()
            }}>
      
                <div  
                className='modal' 
                onClick={(e) => {
                    e.stopPropagation()
                    }}
                style={{backgroundColor : props.background}} 
                    >

                    <div className='modal__content-container'>                             
                            <h3>{props.modalTitle}</h3>
                            <p>{props.modalText}</p>
                            <div className='button-block'>

                            </div>
                    </div>
                            <Btn 
                            className={'modal-btn'}
                             text={props.btnText} 
                            onClick={props.onClick}
                             argument={props.argument}
                            />
                </div>
          </div>
        )
    }



Modal.propTypes = {
    checkItem: PropTypes.object,
    action: PropTypes.func,
    closeButton: PropTypes.bool, 
    modalTitle: PropTypes.string,
    modalText: PropTypes.string,
    onClick: PropTypes.func,
    background: PropTypes.string,
        }

        Modal.defaultProps = {
            background: 'rgb(69, 188, 236)', 
            modalTitle: 'This is title', 
            modalText: 'This is default text'
          };

