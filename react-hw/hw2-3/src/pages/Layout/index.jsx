import { Outlet, NavLink } from "react-router-dom";
import Header from "../../Components/Header/Header";

export default function Layout(props) {
 const [delateOrder, delateWish, checkItem] = props.funcs;
 const [items, wish, orders] = props.state;
    return(
        <>

        <Header 
        funcs={[delateOrder, delateWish, checkItem]}
        state={[items, wish, orders]}
        delateWish={props.delateWish} 
        delateOrder={props.delateOrder} 
        wish={props.wish} 
        orders={props.orders}/>

         <div className="container">
         <Outlet/>  
            </div> 
        </>
    )
}