let tabsBlok = document.querySelector('.tabs');
let tabsList = tabsBlok.querySelectorAll('li');
let outBlock = document.querySelector('.tabs-content');
let outList = outBlock.querySelectorAll('li');


for (let i = 0; i < tabsList.length; i++) {
    tabsList[i].setAttribute('data-attribute', `tab${i}`)
}

for (let i = 0; i < outList.length; i++) {
    outList[i].setAttribute('data-attribute', `tab${i}`);
    }

for (const li of outList) {
    console.log(li.getAttribute('data-attribute'));
    if (li.getAttribute('data-attribute')=== document.querySelector('.active').getAttribute('data-attribute')) {
        li.hidden = false;
    } else{
        li.hidden = true;
    }
}


tabsBlok.addEventListener('click', function (event) {
    for (const li of outList) {
        li.hidden = true;
    }

    for (const li of tabsList) {
        li.classList.remove('active')
    }

    for (const li of outList) {
        li.hidden = true;
    }

    if (!event.target.classList.contains('active')) {
        event.target.classList.add('active')
        let attr = event.target.getAttribute('data-attribute');

        for (const li of outList) {
            if (attr === li.getAttribute('data-attribute')) {
                li.hidden = false;
            }
        }
    }
})


