import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom/client';
import './App.scss'
import './reset.scss'
import Modal from '../Components/Modal/Modal'
import ItemList from '../Components/ItemList/ItemList';
import Header from '../Components/Header/Header';
import { Route, Routes, Navigate, Outlet } from 'react-router-dom';
import About from '../Components/pages/About/About';
import Contacts from '../Components/pages/Contacts/Contacts';
import Favorite from '../Components/pages/Favorite/Favorite';
import CardPage from '../Components/pages/CardPage.jsx/CardPage';
import { BrowserRouter } from "react-router-dom";





export default function App(props) {
    const [secondModalActive, setSecondModalActive] = useState(false);
    const [firstModalActive, setFirstModalActive] = useState(false);

    const [items, setItems] = useState([]);
    const [wish, setWish] = useState([]);
    const [orders, setOrders] = useState([])
    const [SelectItem, setSelectItem] = useState({});
    const [getItem, setcheckItem] = useState({});
    
 
    useEffect(() => {
        fetch('data.json')
        .then(data => data.json())
        .then(data => setItems(data))
        
        // if (localStorage.getItem('orders') !== null) {
        //     setOrders(JSON.parse(localStorage.orders))
        // }
        
        // if (localStorage.getItem('wish') !== null) {
        //     setWish(JSON.parse(localStorage.wish))
        // }
    })


    const showFirstModal = () => {
        setFirstModalActive(!firstModalActive)
    }

    const showSecondModal = () => {
        setSecondModalActive(!secondModalActive)
    }

    const addOrder = (item) => {
        let isInArr = false;
        orders.forEach(el => {
            if (item.id === el.id) {
                isInArr = true
            }
        })
        if (!isInArr) {
            setOrders([...orders, item]);
        }
    }

    const addWish = (item) => {
        let isInArr = false;
        wish.forEach(el => {
            if (item.id === el.id) {
                isInArr = true
            }
        })
        if (!isInArr) {
            setWish([...wish, item])
        }
    }

    const checkItem =  (item) => {
        setcheckItem(item)
    }

    const delateOrder = (item) => {
        let newOrders = []
        orders.forEach(el => {
            if (item.id === el.id) {
                setSelectItem(el.id )
                return
            } else {
                newOrders.push(el)
            }
        })
        setOrders([...newOrders]);
        // localStorage.orders = JSON.stringify(newOrders);
    }

    const delateWish = (item) => {
        let newOrders = []
        wish.forEach(el => {
            if (item.id === el.id) {
                setSelectItem(el.id )
                return
            } else {
                newOrders.push(el)
            }
        })
        setWish([...newOrders])
        // localStorage.wish = JSON.stringify(newOrders);
    }

    return (
        <div>
            <Header 
            funcs={[delateOrder, delateWish, checkItem]}
            state={[items, wish, orders]}
            delateWish={delateWish} 
            delateOrder={delateOrder} 
            wish={wish} 
            orders={orders} />

            <div className='conteiner'>
                <Routes>
                    <Route path="/" element={<ItemList
                    state={[secondModalActive,  items, wish, orders, SelectItem]}
                    funcs={[showSecondModal,showFirstModal, addOrder, addWish, setcheckItem, checkItem, delateWish]}
                    />} />
                    <Route path="favorite" element={<Favorite />} />
                    <Route path="card" element={<CardPage />} />
                    <Route path="about" element={<About />} />
                    <Route path="contacts" element={<Contacts />} />
                </Routes>
            </div>

            {secondModalActive && <Modal 
            closeModal={showSecondModal}
            funcs={[addOrder, showSecondModal]}
            argument={getItem}
            closeButton={false} 
            btnText='Додати товар'
            modalTitle='Додати товар до корзини' 
            modalText={`Якщо бажаєте додати товар ${getItem.category}  ${getItem.name} до корзини, натисніть "Ок"`} 
            background='#d5d5d5' />}

            {firstModalActive && <Modal 
            closeModal={showFirstModal}
            funcs={[addWish, showFirstModal]}
            argument={getItem}
            closeButton={false} 
            btnText='Додати товар'
            modalTitle='Додати товар до корзини' 
            modalText={`Якщо бажаєте додати товар ${getItem.category}  ${getItem.name} до корзини бажань, натисніть "Ок"`} 
            background='#d5d5d5' />}


        </div>
    )

}

