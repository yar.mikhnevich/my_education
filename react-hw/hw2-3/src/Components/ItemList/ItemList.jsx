import React from 'react';
import ReactDOM from 'react-dom/client';
import Item from '../Item/Item';
import './ItemList.scss'
import PropTypes from 'prop-types';



export default function ItemList (props) {
    const [secondModalActive,  items, wish, orders, SelectItem] = props.state

        return(
            <div className='item-list'>
            {
                items.map(item => {
                    const serchBoolean = wish.some((el) => {
                        return el.id === item.id
                    })    

                    return (
                        <Item 
                        favoritBtn={true}
                        addBtn={true}
                        isWish={serchBoolean}
                        state={props.state}
                        key={item.id} 
                        funcs={props.funcs}
                        item={item}/>
                        )
            })}
            </div>
            )        
    }



ItemList.propTypes = {
items: PropTypes.array
}