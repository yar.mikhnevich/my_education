class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary
    }
    get nameEmployee() {
        return this.name;
    }
    set nameEmployee(value) {
        this.name = value;
    }

    get ageEmployee() {
        return this.age;
    }
    set ageEmployee(value) {
        this.age = value ;
    }

    get salaryEmployee() {
        return this.salary;
    }
    set salaryEmployee(value) {
        this.salary = value ;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age);
        this.salary = salary
        this.lang = [...lang];
    }

    set salaryEmployee(value) {
        this.salary = value ;
    }

    get salaryEmployee() {
         return (this.salary * 3)
    }

    
}

let newEmployee = new Employee('Yaroslav', 32, 2500);
let newProgramer = new Programmer('Sasha', 27, 3500, ['english', 'french', 'spanish']);
let newProgramer2 = new Programmer('Igor', 22, 2700, ['english', 'ukrainian', 'spanish']);
let newProgramer3 = new Programmer('Vika', 25, 3000, ['english', 'french', 'Hindi']);

console.log(newEmployee);

console.log(newProgramer);
console.log(newProgramer.salaryEmployee);

console.log(newProgramer2);
console.log(newProgramer2.salaryEmployee);

console.log(newProgramer3);
console.log(newProgramer3.salaryEmployee);
