import React from 'react';
import './Item.scss'
import PropTypes from 'prop-types';
import { BsStarFill } from 'react-icons/bs'
import { MdClose } from 'react-icons/md'
import Btn from '../Btn/Btn';
import "../Btn/Btn.scss"
import { NavLink } from 'react-router-dom';



export default function Item(props) {
    const { name, category, size, price, id, color, url } = props.item;
    const [secondModalActive] = props.state;
    const [showSecondModal, showFirstModal, addOrder, addWish, setcheckItem, checkItem, delateWish, delateOrder, deleteOrderModal] = props.funcs;
    return (
        <div className='item'>
            <div key={id} className='item__content'>
                <div className='item__content--img-block'><img src={url} /></div>
                <div className='item__content--info-block'>
                    <h3 className='item-title'>{category} {name}</h3>
                    <p className='item-price'>{price} грн</p>
                    <p>Артикул: {id}</p>
                    <p>Колір: {color}</p>
                    <NavLink to={`/${id}`}>Детальніше про товар</NavLink>
                </div>
            </div>

            {props.favoritBtn && <button
                onClick={() => {
                    props.isWish ? delateWish(props.item) : showFirstModal()
                    checkItem(props.item)
                }}

                className={`wish-btn ${props.isWish && 'active'}`}

            >
                <BsStarFill />
            </button>}

            {props.closeBtn && <Btn
                className={'close-btn'}
                text={<MdClose />}
                onClick={() => {
                    const [delateWishModalFunc, addOrder, checkItem, delateWish, showFirstModal] = props.funcs
                    delateWishModalFunc()
                    checkItem(props.item)


                }}
            />}


            {props.closeBtnCard && <Btn
                className={'close-btn'}
                text={<MdClose />}
                onClick={() => {
                    const [checkItem, deleteOrderModal] = props.funcs
                    deleteOrderModal()
                    checkItem(props.item)
                }}
            />}

            {props.addBtnWish && <Btn
                className={'card-btn'}
                text={'Додати до корзини з списку бажань'}
                argument={props.item}
                onClick={() => {
                    const [delateWishModalFunc, addOrder, checkItem, delateWish, showSecondModal] = props.funcs
                    showSecondModal()
                    checkItem(props.item)

                }}
            />}

            {props.addBtn && <Btn
                className={'card-btn'}
                text={'Додати до кошика'}
                argument={props.item}
                onClick={() => {
                    showSecondModal()
                    checkItem(props.item)
                }}
            />}

        </div>
    )
}


Item.propTypes = {
    addOrder: PropTypes.func,
    addWish: PropTypes.func,
    checkItem: PropTypes.func,
    delateWish: PropTypes.func,
    showFirstModal: PropTypes.func,
    showSecondModal: PropTypes.func
}


