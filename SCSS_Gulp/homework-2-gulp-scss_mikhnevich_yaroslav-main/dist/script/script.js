let burgerMenu = document.querySelector('.nav-list__burger');
let burgerSide = document.querySelector('.nav-list__burger-side');

burgerMenu.addEventListener('click' , function (event) {
    let target = event.target.closest('li');
    if (target) {
        burgerMenu.classList.toggle('active')
        burgerSide.classList.toggle('active')

    }
})

let burgerBlock = document.querySelector('.nav-list__burger-side');
let burgerItemList = document.querySelectorAll('.nav-list__burger-side--item');
burgerBlock.addEventListener('click' , function (event) {
    for (const item of burgerItemList) {
        item.classList.remove('selected');
    }
    let target = event.target.closest('li');
    if (target) {
        target.classList.add('selected');

    }
})