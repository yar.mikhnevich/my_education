import React from 'react';
import ReactDOM from 'react-dom/client';
import './WishList.scss'
import CardItem from '../CardItem/CardItem';
import PropTypes from 'prop-types';
import {GrClose} from 'react-icons/gr'
import { Link } from 'react-router-dom';


export default function WishList(props) {
    const [delateWish, checkItem, setactiveCard] = props.funcs;
    return (
        <div className='wish-list-block-background'>
            <div className='card-list-block'
                onClick={(e) => {
                    e.stopPropagation()
                }}>
                <ul>
                    {props.wish.length === 0 && <h3>{props.message}</h3>}
                    <GrClose onClick={
                            ()=>{
                                setactiveCard(!props.state)
                            }
                        } className='close-order-window'/>
                    {props.wish.length > 0 && props.wish.map((el) => {
                        return (
                            <CardItem
                                closeBtn={true}
                                key={el.id}
                                item={el}
                                funcs={[delateWish, checkItem]}
                            />
                        )
                    })
                    }
                </ul>
                <Link to='/favorite' className='.tofav-btn' >До списку бажань</Link>
            </div>
        </div>
    )
}


WishList.propTypes = {
    delateOrder: PropTypes.func,
    item: PropTypes.object,
    delateWish: PropTypes.func,
    wish: PropTypes.array,
}

