import { NavLink } from "react-router-dom";
import Item from "../../Components/Item/Item"
import './Favorite.scss'
import PropTypes from 'prop-types';



export default function Favorite(props) {
  const [secondModalActive, items, wish, orders, SelectItem] = props.state;
  return(
    <div className='item-list'>
    {

wish.length > 0 ? wish.map(item => {
            const serchBoolean = wish.some((el) => {
                return el.id === item.id
            })    
            return (
                <Item 
                addBtnWish={true}
                closeBtn={true}
                isWish={serchBoolean}
                state={props.state}
                key={item.id} 
                funcs={props.funcs}
                item={item}
                />
                )
    }): <div className="empty-block">
      <img src="https://xl-static.rozetka.com.ua/assets/img/design/cabinet/cabinet-dummy-error.svg" />
      <p>Упс! Ваш список бажань пустий</p>

      <NavLink className='back-btn' to="/">Повернутися до товарів</NavLink>

    </div>
    
    }
    </div>
    )        
  }


  Favorite.propTypes = {
    wish: PropTypes.array
    }