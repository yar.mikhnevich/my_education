export default class User {
    constructor(name, id, email) {
        this.name = name;
        this.id = id;
        this.email = email;

    }

    render(block) {
        const user = document.createElement('div');
        const name = document.createElement('h3');
        const postBlock = document.createElement('div');
        const mail = document.createElement('span');

        user.classList.add('user-item');

        postBlock.classList.add('posts-block');
        postBlock.id = this.id;

        mail.innerText = `User's mail: ${this.email}`
        name.innerText =`${this.name} twitts`

        user.append(name, mail, postBlock)

        block.append(user)
    }
}