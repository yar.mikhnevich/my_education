
export default class City {
    constructor(cityUA, cityENG, id, src, descriptionUA, descriptionENG) {
        this.cityUA = cityUA;
        this.cityENG = cityENG;
        this.id = id;
        this.src = src;
        this.descriptionUA = descriptionUA;
        this.descriptionENG = descriptionENG;


    }
    renderCity(cityBlock) {
        let cityWrapper = document.createElement('div');
        let btn = document.createElement('button');
        let city = document.createElement('h3');
        let img = document.createElement('img');
        let description = document.createElement('p');
        let overImg = document.createElement('div');
        let infoWrapper = document.createElement('div');
        
        city.innerText = this.cityUA;
        description.innerText = this.descriptionUA;
        img.src = this.src;
        btn.innerText = 'обрати місто';
        cityWrapper.id = this.id;
        
        cityWrapper.classList.add('city-item');
        img.classList.add('city-item__img');
        overImg.classList.add('city-item__info-block');
        city.classList.add('city-item__info-block--title');
        infoWrapper.classList.add('city-item__info-block--wrapper');
        description.classList.add('city-item__info-block--description');
        btn.classList.add('city-item__info-block--btn');
        btn.id = this.id;

        infoWrapper.append(city, description,  btn);
        overImg.append(infoWrapper)
        cityWrapper.append(img,overImg )
        cityBlock.append(cityWrapper);
    }
}