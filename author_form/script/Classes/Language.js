'use strict'

export default class Language {
    constructor(language, src){
        this.language = language;
        this.src = src;
    }


    render(container){
        const item = document.createElement('div');
        item.classList.add('item');
        item.id = this.language;

        item.innerHTML = ` 
        <img src="${this.src}" class="item__img">
        <div class="item__info-block">
        <div class="item__info-block--wrapper">
        <h3 class="item__info-block--title">${this.language}</h3>
        <button class="item__info-block--btn" id="${this.language}">choose language </button>
        </div></div>`;

        container.append(item);
   }   
}