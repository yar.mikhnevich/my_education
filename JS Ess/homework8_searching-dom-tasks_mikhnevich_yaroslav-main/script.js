console.log(`
1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000:`);
let p = document.querySelectorAll('p');
console.log(p);
for (const iterator of p) {
    iterator.style.backgroundColor = '#ff0000';
}

////////////////////////////////////////////
console.log(`
2.1  Знайти елемент із id="optionsList". Вивести у консоль.`);
let elemWithId = document.getElementById('optionsList');
console.log(elemWithId);

////////////////////////////////////////////
console.log(`
2.2  Знайти батьківський елемент та вивести в консоль.`);
let perent = elemWithId.parentElement;
console.log(perent);

////////////////////////////////////////////
console.log(`
2.3  Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.`);
let children = '';
if (elemWithId.childNodes) {
    children = elemWithId.childNodes;
}
for (const iterator of children) {
    console.log(`назва ноди: ${iterator.nodeName}, тип ноди: ${iterator.nodeType}`);
}

////////////////////////////////////////////
console.log(`
3.   Встановіть в якості контента елемента з класом testParagraph 
наступний параграф -This is a paragraph
`);
let testParagraph = document.getElementById('testParagraph');
testParagraph.textContent = 'This is a paragraph';
console.log(testParagraph);
 
////////////////////////////////////////////
console.log(`
4.   Отримати елементи, вкладені в елемент із класом main-header 
і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
`);
let main = document.querySelector('.main-header');
let childOfMain = main.children;
for (const iterator of childOfMain) {
    iterator.classList.add('nav-item');
}
console.log(childOfMain);

////////////////////////////////////////////
console.log(`
5.  Знайти всі елементи із класом section-title. 
Видалити цей клас у цих елементів.
`);
let sectionTitle = document.querySelectorAll('.section-title');
for (const iterator of sectionTitle) {
    iterator.classList.remove('section-title')
}
console.log(sectionTitle);