'use strict'
import  Language from "./Classes/Language.js";
import City from "./Classes/City.js";
// import {renderLang, createCity} from "./Functions/func.js"


function renderLang(data, block) {
    data.forEach(function ({language,src}) {
        new Language(language,src).render(block);  
    })
}
    
function createCity(data, block) {
    data.forEach(function ({cityUA, cityENG, id, src, descriptionUA, descriptionENG}) {
        new City(cityUA, cityENG, id, src, descriptionUA, descriptionENG).renderCity(block);  
    })}




let cityBlock = document.querySelector('.city-block');
let languagesBlock = document.querySelector('.languages-block')

//анімація з логотипом на сторінці авторизації
setTimeout(() => {
    document.querySelector('.logo-block').classList.add('logo-block-hidden');

    setTimeout(() => {
        document.querySelector('.logo-block').style.top = '40%';
    }, 2000);

    setTimeout(() => {
        document.querySelector('.logo-block').classList.add('logo-block-show');
        document.querySelector('.preview-text').style.opacity = '100%';

    }, 3000);
    
    setTimeout(() => {
        document.querySelector('.preview-text').style.opacity = '0';
    }, 7000);

    setTimeout(() => {
        document.querySelector('.next-text').style.opacity = '100%';
    }, 8000);

    setTimeout(() => {
        document.querySelector('.social-networks-list').style.opacity = '100%'
        document.querySelector('.social-networks-list--item-1').style.opacity = '100%'
    }, 9000);

    setTimeout(() => {
        document.querySelector('.social-networks-list--item-2').style.opacity = '100%'
    }, 10000);

    setTimeout(() => {
        document.querySelector('.social-networks-list--item-3').style.opacity = '100%'
    }, 11000);

}, 5000);

document.querySelector('.button-block--log-up').addEventListener('click', (event)=>{
if (event.target.closest('button')) {
document.querySelector('.button-block').style.display = 'none';
document.querySelector('.sing-up-block').style.display = 'flex';

}
})

document.querySelector('.sing-up-block__form--btn').addEventListener('click', (event)=>{
        if (event.target.closest('input')) {
            let pass = document.querySelector('.sing-up-block__form--password').value;
            let mail = document.querySelector('.sing-up-block__form--gmail').value;
            
            fetch('https://raw.githubusercontent.com/YaroslavMikhnevich/data/main/usersdata.json')
            .then((data) => {
                return data.json();
            })
            .then((data) => {
                let checkLog = data.some(({password, gmail})=>{ 
                    console.log('test');
                    return (password == pass ||  gmail == mail);
                    
               }) 
               console.log(checkLog);

               if (checkLog) {
                    document.querySelector('.reg-form-block').style.display = 'none'
                    document.querySelector('.main').style.display = 'block'

                    } else{
                        
                        document.querySelector('.error-block').style.display = 'block'

                    }
               
            })
} 
})




fetch('https://raw.githubusercontent.com/YaroslavMikhnevich/data/main/generaldata.json')
.then((data) => {
    return data.json();
})
.then(({cities, languages}) => {
    createCity(cities, cityBlock)  
    renderLang(languages, languagesBlock)

});




