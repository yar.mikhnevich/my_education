export default class Cat {
    constructor(query, timezone, country, regionName, city, region) {
        this.query = query;
        this.timezone = timezone;
        this.country = country;
        this.regionName = regionName;
        this.city = city;
        this.region = region;
    }


    renderAngryCat() {
        const block = document.createElement('div');
        block.classList.add('cat-block');

        const wrapperImg = document.createElement('div');
        block.classList.add('wrapper-img');



        const angryGif = document.createElement('img');
        angryGif.src = "./img/angry-cat.gif";
        angryGif.classList.add('angry-gif')


        const findByIp = document.createElement('button');
        findByIp.innerText = "find by IP";
        findByIp.classList.add('find-by-ip')




        const messageFromAngryCat = document.createElement('p');
        messageFromAngryCat.innerText = 'I will find you by the IP and clean your mouth';
        messageFromAngryCat.classList.add('message-from-angry-cat')




        wrapperImg.append(angryGif)

        block.append(wrapperImg, messageFromAngryCat, findByIp);

        document.body.append(block);

        findByIp.addEventListener('click', async function (event) {
            if (event.target.closest('button')) {
                block.innerHTML = '';

                const data = await fetch('https://api.ipify.org/?format=json')
                    .then(data => data.json())
                const { ip } = data;
    
                const userData = await fetch(`http://ip-api.com/json/${ip}`)
                    .then(data => data.json());
    
                console.log(userData);
                const { query, timezone, country, regionName, city, region } = userData;
                console.log(query);
                new Cat(query, timezone, country, regionName, city, region).renderCatWhoDrive()
            }
            

        })
    }

    renderCatWhoDrive() {
        const block = document.createElement('div');
        block.classList.add('cat-block');

        const wrapperImg = document.createElement('div');
        block.classList.add('wrapper-img');



        const angryGif = document.createElement('img');
        angryGif.src = "./img/cat-driving-serious.gif";
        angryGif.classList.add('angry-gif')


        const apologizeBtn = document.createElement('button');
        apologizeBtn.innerText = "apologize to the cat";
        apologizeBtn.classList.add('find-by-ip')
        addEventListener('click', function () {

            if (event.target.closest('button')) {
                block.innerHTML = '';
                new Cat().happyCat()
            }

        })


        const messageFromAngryCat = document.createElement('p');
        messageFromAngryCat.innerHTML = `I know where you living.<br>
        Your IP: ${this.query}, 
        <br>
        it's mean that I'm driving to
        <br>
        contenent:${this.timezone}, 
        <br>
        country:${this.country}, 
        <br>
        region: ${this.regionName}, 
        <br>
        city: ${this.city}, 
        <br>
        city area: ${this.region}`;

        messageFromAngryCat.classList.add('message-from-angry-cat')


        wrapperImg.append(angryGif)

        block.append(wrapperImg, messageFromAngryCat, apologizeBtn);

        document.body.append(block);

    }

    happyCat() {
        const block = document.createElement('div');
        block.classList.add('cat-block');

        const wrapperImg = document.createElement('div');
        block.classList.add('wrapper-img');

        const happyGif = document.createElement('img');
        happyGif.src = "./img/happycat.gif";
        happyGif.classList.add('angry-gif')


        const reloadBtn = document.createElement('button');
        reloadBtn.innerText = "Reload";
        reloadBtn.classList.add('find-by-ip');
        reloadBtn.addEventListener('click', function () { 
            if (event.target.closest('button')) {
                location.reload()
            } })

        const messageFromCat = document.createElement('p');
        messageFromCat.innerText = `Don't worry, I'm good now`;
        messageFromCat.classList.add('message-from-angry-cat')

        wrapperImg.append(happyGif)

        block.append(wrapperImg, messageFromCat, reloadBtn);

        document.body.append(block);

    }
}