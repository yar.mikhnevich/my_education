import React from 'react';
import ReactDOM from 'react-dom/client';
import './Button.scss'


export default class Button extends React.Component {
	constructor(props){
		super(props);
	}

    render(){
        return(
            <button
            onClick={this.props.onClick}
            className={this.props.className ?  this.props.className : "button"}
            style={{backgroundColor : this.props.background}}
            >{this.props.text}</button>
        )
    }
}