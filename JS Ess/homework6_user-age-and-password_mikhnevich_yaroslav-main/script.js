function createNewUser() { 
    let firstName = prompt('Enter your name');
    let lastName = prompt('Enter your last name');

    let birthString = prompt('Enter your birthday', 'dd.mm.yyyy');
    let birthArr = birthString.split('.');
    let birthday = new Date(birthArr[2], (birthArr[1] - 1), birthArr[0]);

    return {
    firstName,
    lastName,    
    getLogin () {
        let login = firstName[0].toLowerCase()+ lastName.toLocaleLowerCase();    
        return login;
    },
    getAge(){
        let dateNow = new Date();
        return (dateNow.getFullYear() - birthday.getFullYear())
    }, 
    getPassword(){
        return (firstName[0].toUpperCase() + lastName.toLowerCase() + birthday.getFullYear());
    },

    }
   };


let newUser = createNewUser(); 

Object .defineProperties(newUser , {
    'firstName' : {writable: false},
    'lastName' : {writable: false}, 
});

function setFirstName(newFNam) {
    Object .defineProperties(newUser , {
        'firstName' : {writable: true},
    });
    newUser.firstName = newFNam;
}

function setLastName(newLNam) {
    Object .defineProperties(newUser , {
        'lastName' : {writable: true},
    });
    newUser.lastName = newLNam;
}


console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());