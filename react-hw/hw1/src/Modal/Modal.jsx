import React from 'react';
import ReactDOM from 'react-dom/client';
import './Modal.scss'
import Button from '../Button/Button'




export default class Modal extends React.Component {
	constructor(props){
		super(props);
	}


    render(){
        return(
            <div className='modal-background'  onClick={this.props.onClick}>
                <div  className='modal' style={{backgroundColor : this.props.background}} 
                onClick={(e) => {
                    e.stopPropagation()
                    }}>
                <Button  
                text={<svg
                    xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg"
                    viewBox="0 0 16 16">
                    <path
                        d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z">
                    </path>
                </svg>} 
                onClick={this.props.onClick} 
                className='modal__close-btn'
                />
                    <div className='modal__content-container'>
                          
                            <h3>{this.props.modalTitle}</h3>
                            <p>{this.props.modalText}</p>
                            <div className='button-block'>
                            <Button text='Cancel' background='#ff4c00' onClick={this.props.onClick}  className='modal__btn'/>
                            <Button text='Ok' background='rgb(0, 103, 198)' onClick={this.props.onClick} className='modal__btn'/>
                            </div>
                    </div>
                </div>
          </div>
        )
    }
}