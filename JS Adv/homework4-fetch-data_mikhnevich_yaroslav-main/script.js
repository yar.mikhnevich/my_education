function loaded() {
  setTimeout(() => {
    document.querySelector('.preloader').classList.add('loaded');
    document.body.classList.remove('no-scroll');
  }, 2000);
}
class filmCard {
  constructor({ episodeId, name, openingCrawl }) {
    this.name = name;
    this.episodeId = episodeId;
    this.openingCrawl = openingCrawl
  }

  render() {
    let card = document.createElement('div');
    let title = document.createElement('h2');

    let p = document.createElement('p');
    let h3 = document.createElement('h3');
    let h4 = document.createElement('h4');
    let ul = document.createElement('ul');
    let br = document.createElement('br');

    card.classList.add('film-card');

    title.innerText = `Name of eposode: ${this.name}`;
    h3.innerText = `Number of eposode: ${this.episodeId}`;
    p.innerText = `Shortly in epizode: ${this.openingCrawl}`;
    h4.innerText = `Character in eposode:`;
    ul.id = `${this.episodeId}`;
    card.append(title, h3, p, h4, ul);
    document.body.append(card);

    title.style.marginBottom = '20px';
    h3.style.marginBottom = '10px';
    p.style.marginBottom = '10px';
  }

  renderCharacters({name}) {
    let list = [];
    let li = document.createElement('li');
    li.innerText = `${name}`;
    list.push(li)
    document.getElementById(`${this.episodeId}`).append(...list)
  }
}

fetch('https://ajax.test-danit.com/api/swapi/films')
  .then((data) => {
    return data.json();
  })
  .then((data) => {
    data.forEach((el) => {
      let card = new filmCard(el);
      card.render();

      el.characters.forEach((character) => {
        fetch(character)
          .then((data) => {
            return data.json();
          })
          .then((data) => {
            card.renderCharacters(data)
          })
          .finally(() => {
            loaded()
          })
      })
    })

  })








