let form = document.querySelector('form');
let btn = document.querySelector('.btn');

let enterInput = form.querySelector('.enter-password');
let confirmInput = form.querySelector('.confirm-password');
let message = document.createElement('p');
message.innerText = 'Потрібно ввести однакові значення';
message.style.color = 'red';
message.classList.add('close');
btn.before(message);


form.addEventListener('click', function (event) {
    let faEye = event.target.closest('i');
    let inputWrapper = event.target.parentElement;
    let input = inputWrapper.querySelector('input');
    console.log(inputWrapper.childNode);
    if (!faEye) return;

    if (!form.contains(faEye)) return;

    if (faEye) {
        let closedI = inputWrapper.querySelector('.close');
        let activeI = inputWrapper.querySelector('.active');

        if (closedI.classList.contains('close')) {
            closedI.classList.toggle('active')
            closedI.classList.toggle('close')
        }
        if (activeI.classList.contains('active')) {
            activeI.classList.toggle('close');
            activeI.classList.toggle('active')
        }
        if (input.type === 'password') {
            input.type = 'text'
        } else {
            input.type = 'password'
        }
    }
})

form.addEventListener('submit', function () {
    if (enterInput.value !== confirmInput.value || enterInput.value === '' || confirmInput.value === '') {
        message.classList.remove('close');
        message.classList.add('active')
    } else {
        message.classList.add('close');
        message.classList.remove('active')
        setTimeout(() => {
            alert('You are welcome!')
        }, 0);
    }
})


