// import  Language from "./Classes/Language.js";
// import City from "./Classes/City.js";

// export {renderLang, createCity};

function renderLang(data, block) {
    data.forEach(function ({language,src}) {
        new Language(language,src).render(block);  
    })
}


    
function createCity(data, block) {
    data.forEach(function ({cityUA, cityENG, id, src, descriptionUA, descriptionENG}) {
        new City(cityUA, cityENG, id, src, descriptionUA, descriptionENG).renderCity(block);  
    })}