import User from './User.js'
import Post from './Post.js'


const block = document.querySelector('.container');

(async function() {
    const users = await fetch('https://ajax.test-danit.com/api/json/users')
        .then((data) => {
            return data.json()
        });

        console.log(users);

    users.forEach(({name, id, email}) => {
        new User(name, id, email).render(block)
    })

    const userPosts = await fetch('https://ajax.test-danit.com/api/json/posts')
        .then((data) => {
            return data.json()
        })

    userPosts.forEach(({ userId, body, title, email}) => {
        new Post(userId, body, title, email).renderPost()
    });

})()




document.querySelector('#add-post').addEventListener('click', (event) => {
    const target = event.target;
    if (target.closest('button')) {
        document.querySelector('.add-post-form').style.display = 'block';
        target.style.display = 'none'

    }
})
document.querySelector('.add-post-form').addEventListener('submit', function (event) {
   event.preventDefault() ;
});

document.querySelector('.create-post').addEventListener('click', async (event) => {
    const target = event.target;
    if (target.closest('button')) {
        document.querySelector('#add-post').style.display = 'block';
        target.closest('.add-post-form').style.display = 'none';
        const titlePost = document.querySelector('.input-title').value;
        const text = document.querySelector('.post-text').value;

        const newPost = await fetch("https://ajax.test-danit.com/api/json/posts", {
            method: 'POST',
            body: JSON.stringify({
                userId: 1,
                body: text,
                title: titlePost,
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => {
            if (response.status === 200) {
                return response.json()
            }})

        const { userId, body, title } = newPost;
        new Post(userId, body, title).renderPost()


    }
})



