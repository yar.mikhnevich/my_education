import React from 'react';
import ReactDOM from 'react-dom/client';
import './ItemList.scss'
import App from '../App/App';



export default class ItemList extends React.Component {
	constructor(props){
		super(props);
	}

    render(){
        const {items} = this.props;
        items.map((item) => {
           console.log(item);
         })

         

        return(
            <>
             {items.map((item) => {
                const {category,color, id, name, price, size, url} = item;
                return <div  key={id}>
                <p>{category}</p>
                <p>{name}</p>

                <p>{color}</p>
                <p>{price}</p>

                <p>{size}</p>
                <img src={url} />

                </div>
             })}
            </>
        )
    }
}