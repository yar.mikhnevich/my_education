export default class Item {
    constructor(title, img) {
        this.title = title;
        this.img = img
    }
    renderItem(box) {
        const itemBlock = document.createElement('div');
        itemBlock.classList.add('item-block');

        itemBlock.addEventListener('click', function (event) {
            if (event.target.closest('.item-block')) {
                contentItem.classList.toggle('checked-item');
                if (contentItem.classList.contains('checked-item')) {
                    inputItem.checked = true
                } else {
                    inputItem.checked = false
                }
            }
        })

        const contentItem = document.createElement('div');
        contentItem.classList.add('content-item');

        contentItem.style.backgroundImage = `url(${this.img})`;

        const title = document.createElement('h2');
        title.classList.add('item-title');
        title.innerText = this.title;

        const inputItem = document.createElement('input');
        inputItem.classList.add('item-input');
        inputItem.type = 'checkbox';

        contentItem.append(title)
        itemBlock.append(contentItem, inputItem)
        box.append(itemBlock)

    }
}