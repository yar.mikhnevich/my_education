import React from 'react';
import ReactDOM from 'react-dom/client';
import './CardList.scss'
import CardItem from '../CardItem/CardItem';
import PropTypes from 'prop-types';
import {GrClose} from 'react-icons/gr'
import { Link } from 'react-router-dom';


export default function CardList(props) {
    const [delateOrder, checkItem, setactiveCard] = props.funcs;
        return (
            <div className='card-list-block-background'>
                <div className='card-list-block'
                    onClick={(e) => {
                        e.stopPropagation()
                    }}>
                    <ul>
                        {props.orders.length === 0 && <h3>{props.message}</h3>}
                        <GrClose onClick={
                            ()=>{
                                setactiveCard(!props.state)
                            }
                        } className='close-order-window'/>
                        {props.orders.length > 0 && props.orders.map((el) => {
                            return (
                                <CardItem
                                 key={el.id} 
                                 item={el} 
                                 funcs={[delateOrder, checkItem]}
                                />
                            )
                        })
                        }
                    </ul>
                <Link to='/card' className='.tocard-btn' >До кошика</Link>
                </div>
            </div>
        )
    }


CardList.propTypes = {
    delateOrder: PropTypes.func,
    message: PropTypes.string,
    orders: PropTypes.array,
}


