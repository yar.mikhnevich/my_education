import React from 'react';
import ReactDOM from 'react-dom/client';
import './App.scss'
import Button from '../Button/Button'
import Modal from '../Modal/Modal'
import ItemList from '../ItemList/ItemList';


export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstModalActive: false,
            secondModalActive: false,
            items: []
        }
    }

    componentDidMount() {
        fetch('data.json')
            .then(res => res.json())
            .then((res) => {
                this.setState({items: res})
            })



    }

    showFirstModal = () => {
        this.setState({ firstModalActive: !this.state.firstModalActive })
    }

    showSecondModal = () => {
        this.setState({ secondModalActive: !this.state.secondModalActive })
    }

    render() {
        return (
            <div className='container'>
                {/* <div className='btn-block'>
                    <Button text='second modal' background='#ff4c00' onClick={this.showSecondModal} />
                    <Button text='first modal' background='rgb(0, 103, 198)' onClick={this.showFirstModal} />
                </div>
                {
                    (this.state.secondModalActive === true && <Modal modalTitle='Вітаю в реальному світі! ' modalText='Я розчарую тебе, тобі доведеться багато читати документацію, писати код мінімум 25 годин в день, а потім... тобі скажуть(в кращому випадку) шо ти "джуняра", хоча твоя мамка думає, шо ти вже віндовс 12 написав і зараз 13й дописуєш, а ти плачеш від радості, бо джуняра - то сила капітальна. ' onClick={this.showSecondModal} background='#ff4c00' />) ||
                    (this.state.firstModalActive === true && <Modal modalTitle='Хахаха, це все ще, реальний світ! ' modalText='Бачу, ти, хлопче, шукаєш легкі шляхи! Та я просто заміним фон модального вікна(бо можу). Ти ще досі в реальності , тому зберись і давай йди вже роботу робити!' onClick={this.showFirstModal} background='rgb(0, 103, 198)' />)
                } */}
                <ItemList items={this.state.items} />
            </div>
        )
    }
}