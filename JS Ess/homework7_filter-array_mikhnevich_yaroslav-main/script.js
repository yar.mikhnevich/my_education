let arr = [1, null, false, undefined, 5, 8, 'string', 11, 'string', true];

//Рішення за допомогою filter():
function filterBy(arr, type) {

  return arr.filter( item => {
    if (type === 'null') {
      return item !== null;
    }  else{
    return typeof item !== type;
  }
  }
  )
}
console.log(filterBy(arr, "undefined"));


//Рішення за допомогою forEach():
function filterBy2(arr, type) {
  return arr.forEach(item => { 
    if (type === 'null') {
      return item !== null;
    }  else{
    return typeof item !== type;
  }
  });
}
console.log(filterBy(arr, "boolean"))


