let images = document.getElementsByClassName('image-to-show');
let btnWrapper = document.querySelector('.btn-wrapper');


let index = 0;
let delay = 4000;
let timer = document.getElementById('timer')
let sec = delay / 1000;


let timeOut = setTimeout(showImg);
let showTimer = setTimeout(showSec);

function showImg() {
    if (index === images.length) {
        images[index - 1].classList.remove('active');
        images[index - 1].classList.add('close');
        index = 0
    }

    if (images[index].classList.contains('close')) {
        images[index].classList.remove('close');
        images[index].classList.add('active');

    };

    if (index !== 0) {
        images[index - 1].classList.remove('active');
        images[index - 1].classList.add('close');
    };

    index++

    timeOut = setTimeout(showImg, delay);
}

function showSec() {
    timer.innerText = `${sec} секунд`;
    if (sec === 1) {
        sec = (delay / 1000)+1;
    }
    sec--;
    showTimer = setTimeout(showSec, 1000)
}

btnWrapper.addEventListener('click', function (event) {
    let target = event.target;
    if (target.classList.contains('stop-btn')) {
        document.querySelector('.stop-btn').setAttribute("disabled", "disabled");
        clearTimeout(timeOut);
        clearTimeout(showTimer);
    }
    if (target.classList.contains('go-btn')) {
        document.querySelector('.go-btn').setAttribute("disabled", "disabled");
        setTimeout(showImg, sec * 1000);
        setTimeout(showSec);
    }
})
