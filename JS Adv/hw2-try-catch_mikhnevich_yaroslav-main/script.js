const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

let div = document.createElement('div');
document.querySelector('body').prepend(div);
div.id = 'root';


class notFullInfo extends Error {
    constructor(smth) {
        super()
        this.name = 'notFullInfo';
        this.message = `there are not '${smth}' in list`
    }
}

class ListItem {
    constructor(el) {
        if (el.author === undefined) {
            throw new notFullInfo('author');
        }

        if (el.name === undefined) {
            throw new notFullInfo('name');
        }

        if (el.price === undefined) {
            throw new notFullInfo('price');
        }
        this.el = el,
            this.author = el.author,
            this.name = el.name,
            this.price = el.price,
            this.ul = document.createElement('ul');
    }



    render(cont) {
        this.ul.innerHTML = `
            <li> author: ${this.author}</li> 
            <li> name: ${this.name}</li> 
            <li> price: ${this.price}</li>             
            `;
        cont.append(this.ul);
    }
}

books.forEach(el => {
    try {
        new ListItem(el).render(div);
    } catch (error) {
        if (error.name === 'notFullInfo') {
            console.warn(error);
        } else {
            throw error;
        }
    }
})

