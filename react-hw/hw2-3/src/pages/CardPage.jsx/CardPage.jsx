import Item from "../../Components/Item/Item"
import { NavLink } from "react-router-dom";
import PropTypes from 'prop-types';


export default function CardPage(props) {
  const [secondModalActive, items, wish, orders, SelectItem] = props.state;
  return(
    <div className='item-list'>
    {
        orders.length > 0 ? orders.map(item => {
            return (
                <Item 
                closeBtnCard={true}
                state={props.state}
                key={item.id} 
                funcs={props.funcs}
                item={item}/> 
                )
    }) : <div className="empty-block">
    <img src="https://xl-static.rozetka.com.ua/assets/img/design/modal-cart-dummy.svg" />
    <p>Упс! Кошик порожній</p>
    <NavLink className='back-btn' to="/">Повернутися до товарів</NavLink>
  </div>
  }
    </div>
    )        
  }

  CardPage.propTypes = {
    orders: PropTypes.array
    }