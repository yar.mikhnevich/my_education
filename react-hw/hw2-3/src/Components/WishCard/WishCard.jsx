import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';
import { BsStarFill } from 'react-icons/bs'
import QuantityCard from '../QuantityCard/QuantityCard';
import WishList from '../WishList/WishList';
import PropTypes from 'prop-types';



export default function WishCard(props) {
    const [activeCard, setactiveCard] = useState(false)
    const [delateWish, checkItem] = props.funcs
    const wish = props.wish;

    return (
        <div className='card-block'>
            <button onClick={() => { setactiveCard(!activeCard) }}>
                {!activeCard ? <BsStarFill
                    className={`shopping-bag ${activeCard && 'active'}`} /> : ''}
                {!activeCard && <QuantityCard quantity={wish.length} />}

                {activeCard && <WishList
                    state={activeCard}
                    funcs={[delateWish, checkItem,  setactiveCard]}
                    message={'Упс! Ваш список бажань пустий'}
                    wish={wish} />}
            </button>
        </div>
    )
}



WishCard.propTypes = {
    delateOrder: PropTypes.func,
    delateWish: PropTypes.func,
    wish: PropTypes.array,
        }


